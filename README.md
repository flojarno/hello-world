# Hello World - Spring Boot Application

Ce projet est une application simple "Hello World" utilisant Spring Boot. Il est destiné à démontrer les bases du démarrage avec Spring Boot, notamment la configuration initiale et l'affichage d'un message dans la console.

## Prérequis

Pour utiliser ce projet, vous aurez besoin de :
- Java JDK 17 ou plus récent
- Maven 3.6 ou plus récent

## Installation

Pour commencer, clonez ce dépôt sur votre machine locale en utilisant la commande suivante :

```bash
git clone https://gitlab.com/flojarno/hello-world.git
cd hello-world
```

## Utilisation
Une fois le projet cloné, vous pouvez le lancer en utilisant Maven. Ouvrez un terminal à la racine du projet et exécutez :

```bash
./mvnw spring-boot:run
```